# Gitlab Python Runner

  - Gitlab Python Runner has moved! - https://gitlab.com/cunity/gitlab-emulator/-/tree/master/runner

Gitlab is brilliant, the official gitlab runner is pretty robust and supports all the best new features of gitlab.

But one thing that poses as a bit of an obstacle for those of us with fairly exotic build environments is that the
official gitlab runner is written in Go, and Go (or GCC-go) is not available on every platform.

Systems that are intended as targets are:

  * Solaris 11 (patches to the official runner exist but are hard to apply)
  * HP-UX on Itanium (IA64)
  * AIX 6.1, 7.1 on POWER
  * Linux on POWER
  * x64/x86 Linux (but only for internal testing)

## Supported Systems

| *Platform*          | *Shell*     | *Docker*     | *Artifact Upload*   | *Artifact download/dependencies*   |
| ------------------- | ----------- | ------------ | ------------------- | ---------------------------------- |
| Linux (amd64)       | yes         | yes          | yes                 | yes                                |
| Linux (power)       | yes         | maybe        | yes                 | yes                                |
| Windows 10/2019     | yes         | yes          | yes                 | yes                                |
| Solaris 11 (amd64)  | yes         | n/a          | yes                 | yes                                |
| Solaris 11 (sparc)  | yes         | n/a          | yes                 | yes                                |
| AIX 7.1 (python3)   | yes         | n/a          | yes                 | yes                                |
| AIX 7.2 (python3)   | yes         | n/a          | yes                 | yes                                |
| HPUX-11.31 (python3)| yes         | n/a          | yes                 | yes                                |

There really is no sensible reason to use `gilab-python-runner` on x64 linux other than to test changes to the project.

